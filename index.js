const webpack = require('webpack');
const middleware = require('webpack-dev-middleware');
const compiler = webpack(require("./webpack.config"));
compiler.apply(new webpack.ProgressPlugin());
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const path = require("path");

if (process.env.NODE_ENV !== "production") {
    app.use(
        middleware(compiler, {
            // webpack-dev-middleware options
        })
    );
} else {
    app.get("/", (req, res) => {
        res.sendFile(path.join(__dirname, "dist", "index.html"));
    });
    app.get("/app.bundle.js", (req, res) => {
        res.sendFile(path.join(__dirname, "dist", "app.bundle.js"));
    });
}

app.use(express.static("app/js"))

app.listen(port, "0.0.0.0", () => console.log(`Example app listening on port ${port}!`));